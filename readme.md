# RBS PHP Docker Images Build #

### How To Build ###
```sh
cd <folder_name>
```

To build FPM version:
```sh
docker-compose -f fpm-compose.yml up --build
```

To build CLI version:
```sh
docker-compose -f cli-compose.yml up --build
```

To build CLI with Zend Thread-Safe version:
```sh
docker-compose -f zts-compose.yml up --build
```

Change or modify dockerfile inside each version folder.

Start x86/x64 php builds with `series_`

Start arm php builds with `arm_`