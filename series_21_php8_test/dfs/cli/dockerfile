FROM microsoft/mssql-tools as mssql
FROM php:7.4.13-cli-alpine3.12

ARG	PHPREDIS_VERSION="${PHPREDIS_VERSION:-5.3.1}"
ENV	PHPREDIS_VERSION="${PHPREDIS_VERSION}"

# Requirement for Redis extension/
ADD	https://github.com/phpredis/phpredis/archive/${PHPREDIS_VERSION}.tar.gz /tmp/

COPY	--from=mssql /opt/microsoft/ /opt/microsoft/
COPY	--from=mssql /opt/mssql-tools/ /opt/mssql-tools/
COPY	--from=mssql /usr/lib/libmsodbcsql-13.so /usr/lib/libmsodbcsql-13.so

RUN	mkdir -p /usr/local/lib/php/extensions/no-debug-non-zts-20190902
ADD	https://static.rbs1518.net/files/ixed.7.4.lin /usr/local/lib/php/extensions/no-debug-non-zts-20190902

# MSSQL ODBC for DB connection
RUN	curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/msodbcsql17_17.5.2.1-1_amd64.apk \
	&& curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/mssql-tools_17.5.2.1-1_amd64.apk \
	&& apk add --allow-untrusted msodbcsql17_17.5.2.1-1_amd64.apk \
	&& apk add --allow-untrusted mssql-tools_17.5.2.1-1_amd64.apk

# Required for any extension(s).
RUN	apk update \
	&& apk upgrade \
	&& docker-php-source extract \
	\
	&& { \
		echo '# https://github.com/docker-library/php/issues/103#issuecomment-271413933'; \
		echo 'AC_DEFUN([PHP_ALWAYS_SHARED],[])dnl'; \
		echo; \
		cat /usr/src/php/ext/odbc/config.m4; \
	} > temp.m4 \
	&& mv temp.m4 /usr/src/php/ext/odbc/config.m4 \
	\
	&& apk add --update --virtual .build-dependencies \
		$PHPIZE_DEPS \
        git \
		autoconf \
		gcc \
		g++ \
		libtool \
		make \
		pcre-dev \
		coreutils \
		build-base \
		zeromq-dev \
		cyrus-sasl-dev \
		gettext-dev \
		icu-dev \
		libxml2-dev \
		postgresql-dev \
		freetype-dev \
		libjpeg-turbo-dev \
		libpng-dev \
		gmp-dev \
		libmemcached-dev \
		imagemagick-dev \
		libzip-dev \
		zlib-dev \
		libssh2-dev \
		libexif-dev \
		libxslt-dev \
		libevent-dev \
		openssl-dev \
		imap-dev \
		bzip2-dev \
		openldap-dev \
		unixodbc-dev \
		freetds-dev \
		lua-dev \
	\
	&& apk add --no-cache \		
		tini \
		libintl \		
		icu \		
		gmp \		
		libssh2 \		
		libzmq \		
		zeromq \
		net-snmp-dev \
		libuuid \
		freetds \
		lua \
	\
	&& tar xfz /tmp/${PHPREDIS_VERSION}.tar.gz \
	&& mv phpredis-$PHPREDIS_VERSION /usr/src/php/ext/redis \
	\
	&& git clone https://github.com/php-memcached-dev/php-memcached.git /usr/src/php/ext/memcached/ \
	&& docker-php-ext-configure memcached \
	\
	&& docker-php-ext-configure gd \
		--with-freetype \
		--with-jpeg \
	\
	&& docker-php-ext-install bcmath \
	&& docker-php-ext-install calendar \
	&& docker-php-ext-install xsl \
	\
	&& docker-php-ext-configure zip \
	&& docker-php-ext-install zip \
	\
	&& docker-php-ext-install soap \
	&& docker-php-ext-install mysqli \	
	&& docker-php-ext-install intl \
	\
	&& docker-php-ext-install pgsql \
	\	
	&& docker-php-ext-install sockets \
	&& docker-php-ext-install exif \
	&& docker-php-ext-install gettext \
	&& docker-php-ext-install bz2 \
	\
	&& PHP_OPENSSL=yes docker-php-ext-configure imap --with-imap --with-imap-ssl \
	&& docker-php-ext-install imap \
	\
	&& docker-php-ext-install gmp \
	&& docker-php-ext-install redis \
	&& docker-php-ext-install gd \
	&& docker-php-ext-install memcached \
	&& docker-php-ext-install snmp \
	&& docker-php-ext-install xmlrpc \
	&& docker-php-ext-install pcntl \
	\
	&& docker-php-ext-configure ldap \
	&& docker-php-ext-install ldap \
	\
	&& docker-php-ext-configure opcache --enable-opcache \
	&& docker-php-ext-install opcache \
	\
	&& docker-php-ext-install pdo \
	&& docker-php-ext-install pdo_mysql \
	&& docker-php-ext-install pdo_pgsql \
	&& docker-php-ext-install pdo_dblib \
	\
	&& docker-php-ext-configure odbc --with-unixODBC=shared,/usr \	
	&& docker-php-ext-install odbc \
	&& docker-php-ext-configure pdo_odbc --with-pdo-odbc=unixODBC,/usr \
	&& docker-php-ext-install pdo_odbc \		
	\
	&& touch /usr/local/etc/php/bogus.ini \
	&& pear config-set php_ini /usr/local/etc/php/bogus.ini \
	&& pecl config-set php_ini /usr/local/etc/php/bogus.ini \
	&& pecl install event \
	&& docker-php-ext-enable event \
	&& mv /usr/local/etc/php/conf.d/docker-php-ext-event.ini \
		/usr/local/etc/php/conf.d/docker-php-ext-zz-event.ini \
	&& rm /usr/local/etc/php/bogus.ini \
	\
	&& pecl install apcu imagick mongodb \
	&& docker-php-ext-enable apcu imagick mongodb \
	\
	&& pecl install lua \
	&& docker-php-ext-enable lua \
	\
 	&& pecl install sqlsrv pdo_sqlsrv \
 	&& docker-php-ext-enable --ini-name 30-sqlsrv.ini sqlsrv \
 	&& docker-php-ext-enable --ini-name 35-pdo_sqlsrv.ini pdo_sqlsrv \
	\
	&& git clone https://github.com/zeromq/php-zmq.git \	
	&& pecl install php-zmq/package.xml \
	&& docker-php-ext-enable zmq \
	&& rm -R php-zmq \
	\
	&& mkdir /usr/templib \
	&& cp /usr/lib/*.so* /usr/templib \
	\
	&& apk del postgresql-libs libsasl db \
	\
	&& apk del .build-dependencies \
	&& docker-php-source delete \
	\
	&& cp /usr/templib/*.* /usr/lib \
	&& rm -R /usr/templib \
	\
	&& rm -rf /tmp/* /var/cache/apk/* \
	\
	&& touch /usr/local/etc/php/conf.d/php_sg.ini \
	&& echo "extension=ixed.7.4.lin" > /usr/local/etc/php/conf.d/php_sg.ini
