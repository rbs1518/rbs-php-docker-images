FROM	php:7.3.5-fpm-alpine3.9

ARG	PHPREDIS_VERSION="${PHPREDIS_VERSION:-4.2.0}"
ENV	PHPREDIS_VERSION="${PHPREDIS_VERSION}"

# Requirement for Redis extension/
ADD	https://github.com/phpredis/phpredis/archive/${PHPREDIS_VERSION}.tar.gz /tmp/

# Required for any extension(s).
RUN	apk update \
	&& apk upgrade \
	&& docker-php-source extract \
	&& apk add --update --virtual .build-dependencies \
		$PHPIZE_DEPS \
                git \
		autoconf \
		gcc \
		g++ \
		libtool \
		make \
		pcre-dev \
		coreutils \
		build-base \
		zeromq-dev \
		cyrus-sasl-dev \
		gettext-dev \
	\
	&& apk add --no-cache \		
		tini \
		libintl \		
		icu \
		icu-dev \
		libxml2-dev \
		postgresql-dev \
		freetype-dev \
		libjpeg-turbo-dev \
		libpng-dev \
		gmp \
		gmp-dev \
		libmemcached-dev \
		imagemagick-dev \
		libzip-dev \
		zlib-dev \
		libssh2 \
		libssh2-dev \
		libexif-dev \
		libxslt-dev \
		libevent-dev \
		openssl-dev \
		imap-dev \
		bzip2-dev \
		openldap-dev \
		net-snmp-dev \
		libzmq \		
		zeromq \
	\
	&& tar xfz /tmp/${PHPREDIS_VERSION}.tar.gz \
	&& mv phpredis-$PHPREDIS_VERSION /usr/src/php/ext/redis \
	\
	&& git clone https://github.com/php-memcached-dev/php-memcached.git /usr/src/php/ext/memcached/ \
	&& docker-php-ext-configure memcached \
	\
	&& docker-php-ext-configure gd \
		--with-freetype-dir=/usr/include/ \
		--with-jpeg-dir=/usr/include/ \
	\
	&& docker-php-ext-install bcmath \
	&& docker-php-ext-install calendar \
	&& docker-php-ext-install xsl \
	\
	&& docker-php-ext-configure zip --with-libzip \
	&& docker-php-ext-install zip \
	\
	&& docker-php-ext-install soap \
	&& docker-php-ext-install mysqli \	
	&& docker-php-ext-install intl \
	\
	&& docker-php-ext-install pgsql \
	&& docker-php-ext-install pdo_pgsql \
	&& apk del \
		postgresql-libs \
		libsasl \
		db \
	\
	&& docker-php-ext-install sockets \
	&& docker-php-ext-install exif \
	&& docker-php-ext-install gettext \
	&& docker-php-ext-install bz2 \
	\
	&& docker-php-ext-configure imap --with-imap --with-imap-ssl \
	&& docker-php-ext-install imap \
	\
	&& docker-php-ext-install gmp \
	&& docker-php-ext-install redis \
	&& docker-php-ext-install gd \
	&& docker-php-ext-install memcached \
	&& docker-php-ext-install snmp \
	&& docker-php-ext-install xmlrpc \
	\
	&& docker-php-ext-configure ldap \
	&& docker-php-ext-install ldap \
	\
	&& docker-php-ext-configure opcache --enable-opcache \
	&& docker-php-ext-install opcache \	
	\
	&& touch /usr/local/etc/php/bogus.ini \
	&& pear config-set php_ini /usr/local/etc/php/bogus.ini \
	&& pecl config-set php_ini /usr/local/etc/php/bogus.ini \
	&& pecl install event \
	&& docker-php-ext-enable event \
	&& mv /usr/local/etc/php/conf.d/docker-php-ext-event.ini \
		/usr/local/etc/php/conf.d/docker-php-ext-zz-event.ini \
	&& rm /usr/local/etc/php/bogus.ini \
	\
	&& pecl install apcu imagick \
	&& docker-php-ext-enable apcu imagick \
	\
	&& git clone https://github.com/zeromq/php-zmq.git \
	&& cd php-zmq \
	&& pecl install package.xml \
	&& docker-php-ext-enable zmq \
	\
	&& apk del .build-dependencies \
	&& docker-php-source delete \
	\
	&& rm -rf /tmp/* /var/cache/apk/*